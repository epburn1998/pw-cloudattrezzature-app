import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-lista-utenti',
  templateUrl: './lista-utenti.page.html',
  styleUrls: ['./lista-utenti.page.scss'],
})
export class ListaUtentiPage implements OnInit {

  constructor(private router:Router,private api:ApiServiceService) {}

  listaUtenti= [];

  ngOnInit() {
    var dati;
    this.listaUtenti=[]
    this.api.getData().subscribe(res=>{
      res.forEach(element => {
        if(element.IDSquadra == Number(localStorage.getItem("userTeam")))
        {
          this.listaUtenti.push(new utente(element.nomeUtente,element.cognomeUtente
            ,element.IDSquadra,element.ruoloUtente,element.isCaposquadra))
        }
      });
    });
    console.log(this.listaUtenti)

    //
  }

  async checkIfCaposquadra(idUtente:number){
    var listaUtenti =await  this.api.getUtentiInSquadra().pipe().toPromise();
    if (listaUtenti.filter(x=>x.IDUtente == idUtente).isCaposquadra === true){
      return true;
    }
    else {
      return false;
    }
  }

  
}


 class utente{
  constructor(nomeUtente:string,cognomeUtente:string,idSquadra:number,ruoloUtente:string,isCaposquadra:boolean) {
    this.nomeUtente = nomeUtente;
    this.cognomeUtente = cognomeUtente;
    this.IDSquadra=idSquadra;
    this.ruoloUtente=ruoloUtente;
    this.isCaposquadra = isCaposquadra;
  }

  nomeUtente:string;
  cognomeUtente:string;
  IDSquadra:number;
  ruoloUtente:string;
  isCaposquadra:boolean;
}


