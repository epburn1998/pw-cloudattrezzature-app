import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssegnaAttrezzaturaPageRoutingModule } from './assegna-attrezzatura-routing.module';

import { AssegnaAttrezzaturaPage } from './assegna-attrezzatura.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssegnaAttrezzaturaPageRoutingModule
  ],
  declarations: [AssegnaAttrezzaturaPage]
})
export class AssegnaAttrezzaturaPageModule {}
