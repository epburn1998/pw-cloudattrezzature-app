import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Attrezzatura } from '../pagina-lista-audit/pagina-lista-audit.page';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-assegna-attrezzatura',
  templateUrl: './assegna-attrezzatura.page.html',
  styleUrls: ['./assegna-attrezzatura.page.scss'],
})
export class AssegnaAttrezzaturaPage implements OnInit {

  constructor(private apiService:ApiServiceService,private router:Router,private toastCtr:ToastController) { }

  listaAttrezzature: Attrezzatura[];

  async ngOnInit() {
    var audits = await this.apiService.getAudits().pipe().toPromise();
    var attrezzatura = await this.apiService.getAttrezzatureInSquadra().pipe().toPromise();
    var listaAtt = await this.apiService.getAttrezzature().pipe().toPromise();    
  
    localStorage.removeItem("ArrayLista");
    
    this.listaAttrezzature = [];

    if(localStorage.getItem("ArrayLista")!= null) //c'era giá una lista salvata
    {
      console.log("array list piena!");
      this.listaAttrezzature = JSON.parse(localStorage.getItem("ArrayLista"));
    }
    else //é stata aperta una nuova pagina 
    {
      console.log("array list vuota...")
      let i=0;
      attrezzatura.forEach(element => {

        //element.esitoAttrezzatura = audits[i].esitoAuditControllo;
        //element.checkVisivoStrumentale = audits[i].checkVisivoStrumentale;
        var audit = audits.filter(x=>x.IDAssegnazione == element.IDAssegnazione);

        if(audit.length>0){

          var idAudit = audit[0].IDAudit; 




          var att = listaAtt.filter(x=>x.codiceAttrezzatura == element.codiceAttrezzatura)

          console.log(att[0].nomeAttrezzatura)

          var nomeAtt = att[0].nomeAttrezzatura;

        

        



          if(element.IDSquadra == Number(localStorage.getItem("userTeam"))){

            this.listaAttrezzature.push(new Attrezzatura(idAudit,element.idAssegnazione,element.IDSquadra

              ,element.codiceAttrezzatura,att[0].nomeAttrezzatura,audit.esitoAuditControllo

              ,audit.checkVisivoStrumentale,"","",""))

         

          }

        }

        else 

        {

          var att = listaAtt.filter(x=>x.codiceAttrezzatura == element.codiceAttrezzatura)

          console.log(att[0].nomeAttrezzatura)

          var nomeAtt = att[0].nomeAttrezzatura;

        

        



          if(element.IDSquadra == Number(localStorage.getItem("userTeam"))){

            this.listaAttrezzature.push(new Attrezzatura(0,element.idAssegnazione,element.IDSquadra

              ,element.codiceAttrezzatura,att[0].nomeAttrezzatura,"Al momento non audit non richiesto"

              ,false,"","",""))

        }

      }
        
      });
      
      localStorage.setItem("ArrayLista",JSON.stringify(this.listaAttrezzature));
    }
  
  
  }

  cambioAttrezzaturaDetails(codAttrezzatura:number)
  {
    this.apiService.getAttrezzatura(codAttrezzatura).subscribe(res=>{
      if(res.codiceAttrezzatura!=null)
      {
        localStorage.setItem('attrezzaturaCodice',JSON.stringify(res.codiceAttrezzatura));
        localStorage.setItem('nomeAttrezzatura',JSON.stringify(res.nomeAttrezzatura));
        
        var idAss = this.listaAttrezzature.find(x=>x.codiceAttrezzatura===res.codiceAttrezzatura).idAssegnazione;
        localStorage.setItem('idAssegnazione',JSON.stringify(idAss));
         this.router.navigateByUrl('/assegna-attrezzatura-squadra', { state: { codiceattrezzatura:res.codiceAttrezzatura,nomeAttrezzatura:res.nomeAttrezzatura} });
      }
      else
      {
        this.presentToast("Non é stata trovata nessuna attrezzatura con questo codice nel database. Usa un altro codice o contatta il fornitore");
      }
    },(error:any)=>{
      this.presentToast("Errore di connessione. Riprova piú tardi")
    }) 
  }

  async presentToast(msg:string) 
  {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }


}
