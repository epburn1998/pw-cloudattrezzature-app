import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssegnaAttrezzaturaPage } from './assegna-attrezzatura.page';

const routes: Routes = [
  {
    path: '',
    component: AssegnaAttrezzaturaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssegnaAttrezzaturaPageRoutingModule {}
