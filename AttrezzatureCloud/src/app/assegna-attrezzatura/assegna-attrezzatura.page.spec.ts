import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssegnaAttrezzaturaPage } from './assegna-attrezzatura.page';

describe('AssegnaAttrezzaturaPage', () => {
  let component: AssegnaAttrezzaturaPage;
  let fixture: ComponentFixture<AssegnaAttrezzaturaPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AssegnaAttrezzaturaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssegnaAttrezzaturaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
