import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassDimenticataPage } from './pass-dimenticata.page';

const routes: Routes = [
  {
    path: '',
    component: PassDimenticataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PassDimenticataPageRoutingModule {}
