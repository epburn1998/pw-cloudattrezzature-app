import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PassDimenticataPageRoutingModule } from './pass-dimenticata-routing.module';

import { PassDimenticataPage } from './pass-dimenticata.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PassDimenticataPageRoutingModule
  ],
  declarations: [PassDimenticataPage]
})
export class PassDimenticataPageModule {}
