import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeGuardGuard } from '../guards/home-guard.guard';
import { IndexGuardGuard } from '../guards/index-guard.guard';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    canActivate:[IndexGuardGuard],
    component: HomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
