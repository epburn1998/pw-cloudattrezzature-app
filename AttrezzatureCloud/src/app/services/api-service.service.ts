import { HttpClient,HttpClientModule,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiServiceService {

  constructor(private http:  HttpClient) { }

   serviceBaseUrl:string  = 'https:localhost:44346';

   getData(){
    let apiUrl = `${this.serviceBaseUrl}/api/UtenteAPI`;  
    return this.http.get<any>(apiUrl);
  }


  createUser(nome:String,cognome:String,email:String,pass:String){
    let apiUrl = `${this.serviceBaseUrl}/api/UtenteAPI`;
    return this.http.post<any>(apiUrl, { nomeUtente: nome
      ,cognomeUtente:cognome
      ,emailUtente:email
      ,passUtente:pass });
    }

  getAttrezzature(){
    let apiUrl = `${this.serviceBaseUrl}/api/AttrezzaturaAPI/`;
    return this.http.get<any>(apiUrl);
  }

  getAttrezzatura(codAttrezzatura:number)
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AttrezzaturaAPI/`+codAttrezzatura;
    return this.http.get<any>(apiUrl);
  }

  getAttrezzatureInSquadra()
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AttrezzaturaInSquadraAPI/`;
    return this.http.get<any>(apiUrl);
  }

  getSquadre()
  {
    let apiUrl = `${this.serviceBaseUrl}/api/SquadraAPI`;
    return this.http.get<any>(apiUrl);
  }

  assegnaAttrezzoSquadra(idAss:number,codAtt:number,idSquadra:number)
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AttrezzaturaInSquadraAPI/`+idAss;
    return this.http.put(apiUrl,{idAssegnazione:idAss
      ,codiceAttrezzatura:codAtt
      ,IDSquadra:idSquadra});
  }

  

  sendAudit(idAudit:number)
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AuditAPI/`+idAudit; 
    //Url dell'API
    var lista = JSON.parse(localStorage.getItem("ArrayLista"))
    console.log(lista);
    return this.http.put<any>(apiUrl,lista);
  }

  getAudit(idAudit:number)
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AuditAPI/`+idAudit;
    return this.http.get<any>(apiUrl);
  }

  getSingleAudits()
  {
    let apiUrl = `${this.serviceBaseUrl}/api/AuditAPI/`;
    return this.http.get<any>(apiUrl);
  }

  getAudits(){
    let apiUrl = `${this.serviceBaseUrl}/api/AuditSquadraAPI`
    return this.http.get<any>(apiUrl);
  }

  getUtentiInSquadra(){
    let apiUrl = `${this.serviceBaseUrl}/api/AuditAPI/`;
    return this.http.get<any>(apiUrl);
  }


}

 
