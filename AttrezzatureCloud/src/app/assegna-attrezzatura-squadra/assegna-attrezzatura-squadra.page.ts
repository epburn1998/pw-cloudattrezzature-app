import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-assegna-attrezzatura-squadra',
  templateUrl: './assegna-attrezzatura-squadra.page.html',
  styleUrls: ['./assegna-attrezzatura-squadra.page.scss'],
})
export class AssegnaAttrezzaturaSquadraPage implements OnInit {

  constructor(private apiService:ApiServiceService,private toastCtr:ToastController,private router:Router) { }

  private listaSquadre=[];
  nomeAttrezzatura:string;
  codiceAttrezzatura:number;

  idSquadra:number;
  idSquadraToChange:number;
  nomeSquadra:string;
  idAssegnazione:number;
  

  async ngOnInit() {
    this.nomeAttrezzatura= localStorage.getItem("nomeAttrezzatura");
    this.codiceAttrezzatura = Number(localStorage.getItem("attrezzaturaCodice"));

    var attSquad = [];
    attSquad = await this.apiService.getAttrezzatureInSquadra().pipe().toPromise();
    console.log(attSquad);
    var squad = attSquad.find(x=>x.codiceAttrezzatura === this.codiceAttrezzatura);
    this.idSquadra = squad.IDSquadra;
    this.idAssegnazione = squad.IDAssegnazione;
    console.log(this.idAssegnazione);
    console.log(this.idSquadra);
    
    localStorage.removeItem("nomeAttrezzatura");
    localStorage.removeItem("codiceAttrezzatura");
    
    this.listaSquadre=[];
    this.listaSquadre = await this.apiService.getSquadre().pipe().toPromise();
    console.log(this.listaSquadre);
  }

  async presentToast(msg:string) 
  {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async cambiaSquadra(){
    console.log(this.idSquadraToChange)
    if(this.idSquadra == this.idSquadraToChange)
    {
      //stessa squadra di prima!
      this.presentToast("L'attrezzo é giá assegnato a questa squadra!")
    }
    else
    {

      await this.apiService.assegnaAttrezzoSquadra(this.idAssegnazione,this.codiceAttrezzatura,this.idSquadraToChange).pipe().toPromise();
      this.router.navigate(["/menu-principale-links"]);
    }
    
  }

}
