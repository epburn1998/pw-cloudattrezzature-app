import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssegnaAttrezzaturaSquadraPageRoutingModule } from './assegna-attrezzatura-squadra-routing.module';

import { AssegnaAttrezzaturaSquadraPage } from './assegna-attrezzatura-squadra.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssegnaAttrezzaturaSquadraPageRoutingModule
  ],
  declarations: [AssegnaAttrezzaturaSquadraPage]
})
export class AssegnaAttrezzaturaSquadraPageModule {}
