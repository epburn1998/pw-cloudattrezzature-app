import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssegnaAttrezzaturaSquadraPage } from './assegna-attrezzatura-squadra.page';

describe('AssegnaAttrezzaturaSquadraPage', () => {
  let component: AssegnaAttrezzaturaSquadraPage;
  let fixture: ComponentFixture<AssegnaAttrezzaturaSquadraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AssegnaAttrezzaturaSquadraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssegnaAttrezzaturaSquadraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
