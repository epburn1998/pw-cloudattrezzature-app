import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssegnaAttrezzaturaSquadraPage } from './assegna-attrezzatura-squadra.page';

const routes: Routes = [
  {
    path: '',
    component: AssegnaAttrezzaturaSquadraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssegnaAttrezzaturaSquadraPageRoutingModule {}
