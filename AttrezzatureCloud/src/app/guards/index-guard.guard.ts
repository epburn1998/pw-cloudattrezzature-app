import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IndexGuardGuard implements CanActivate {

  constructor(private router:Router){}

  readonly asyncLocalStorage = {
    setItem: async function (key, value) {
        await null;
        return localStorage.setItem(key, value);
    },
    getItem: async function (key) {
        await null;
        return localStorage.getItem(key);
    }
  };
  
    canActivate(): Promise<boolean>{
     return new Promise(resolve=>{
         this.asyncLocalStorage.getItem("userEmail").then(res=>{
          if(res){
            this.router.navigate(['menu-principale']);
            resolve(false);
          }
          else{
            resolve(true);
          }
        })
        .catch(err=>{
          resolve(false);
          
        })
     })
    }
  
}
