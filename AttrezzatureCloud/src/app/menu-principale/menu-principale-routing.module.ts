import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeGuardGuard } from '../guards/home-guard.guard';

import { MenuPrincipalePage } from './menu-principale.page';

const routes: Routes = [
  {
    path: '',
    //canActivate:[HomeGuardGuard],
    component: MenuPrincipalePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPrincipalePageRoutingModule {}
