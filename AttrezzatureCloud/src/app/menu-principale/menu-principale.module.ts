import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPrincipalePageRoutingModule } from './menu-principale-routing.module';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx'

import { MenuPrincipalePage } from './menu-principale.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPrincipalePageRoutingModule,
  ],
  declarations: [MenuPrincipalePage]
})
export class MenuPrincipalePageModule {}
