import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BarcodeScanner, BarcodeScanResult} from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-menu-principale',
  templateUrl: './menu-principale.page.html',
  styleUrls: ['./menu-principale.page.scss'],
})
export class MenuPrincipalePage implements OnInit {

  emailUtente: string;
  codiceScansionato: string;

  auditMessage: string="";
  noOngoingAuditsMessage: string="Al momento nessun fornitore non ha richiesto audit.";
  ongoingAuditsMessage: string="Un fornitore ha richiesto un audit di recente. Apri il menú apposito per piú dettagli.";

  constructor(private barcodeScanner: BarcodeScanner,private router:Router,private toast:ToastController,private apiService:ApiServiceService) { }

  isButtonDisabled(){
    if ( this.auditMessage === this.ongoingAuditsMessage)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  async ngOnInit() {
    this.emailUtente = localStorage.getItem('userEmail');
    console.log("entra")
    //GET per l'audit 
    //prende l'id della squadra e fa una richiesta con quell'id
    //al momento mettiamo un placeholder
    var audits = await this.apiService.getSingleAudits().pipe().toPromise();
    var auditSquadra = await this.apiService.getAudits().pipe().toPromise();

    var auditToCheck=[];

    console.log("f")
    auditSquadra.forEach(element => {
      
      if(element.IDSquadra === Number(localStorage.getItem("userTeam")))
      {
        if(element.esitoAuditControllo=="Attesa")
        {
          this.auditMessage=this.ongoingAuditsMessage;
        }
        else
        {
          this.auditMessage=this.noOngoingAuditsMessage;
        }
      }
      else
      {
        this.auditMessage=this.noOngoingAuditsMessage;
      }
      
    });

    
  }

  async scanCodice(){
    this.barcodeScanner.scan().then(data=>{
        if(data.text==="")
        {
          //non fare nulla
          this.presentToast("Non é stato scannerizzato niente. Riprova.")
        }
        else  //stringa non vuota
        {
          //alert(JSON.stringify(data.text));
          if(!isNaN(Number(data.text)))
          {
             this.apiService.getAttrezzatura(Number(data.text)).subscribe(res=>{
              if(res.codiceAttrezzatura!=null)
              {
                this.router.navigateByUrl('/pagina-dati-audit', { state: { codiceattrezzatura:res.codiceAttrezzatura,nomeAttrezzatura:res.nomeAttrezzatura} });
              }
              else
              {
                this.presentToast("Non é stata trovata nessuna attrezzatura con questo codice nel database. Usa un altro codice o contatta il fornitore");
              }
            })
          }
          else
          {
            this.presentToast("Codice QR inserito non valido! Riprova, o contatta il fornitore.")
          }
          //this.router.navigateByUrl('/paginaDatiAudit', { state: { codiceattrezzatura:data.text} });
        }
        


      },(error:any)=>{
        this.presentToast("Errore di connessione. Riprova piú tardi")
      })
  }

  async debugGetAttrezzatura()
  {
     this.apiService.getAttrezzatura(1).subscribe(res=>{
      if(res.codiceAttrezzatura!=null)
      {
        console.log(res.nomeAttrezzatura);
        localStorage.setItem('attrezzaturaCodice',JSON.stringify(res.codiceAttrezzatura));
        localStorage.setItem('nomeAttrezzatura',JSON.stringify(res.nomeAttrezzatura));
         this.router.navigateByUrl('/pagina-dati-audit', { state: { codiceattrezzatura:res.codiceAttrezzatura,nomeAttrezzatura:res.nomeAttrezzatura} });
      }
      else
      {
        this.presentToast("Non é stata trovata nessuna attrezzatura con questo codice nel database. Usa un altro codice o contatta il fornitore");
      }
    },(error:any)=>{
      this.presentToast("Errore di connessione. Riprova piú tardi")
    })
  }

  

  paginaLista(){
    this.router.navigate(["/pagina-lista-audit"])
  }

  paginaAssegna(){
    this.router.navigate(["/assegna-attrezzatura"])
  }

  paginaUtenti(){
    this.router.navigate(["/lista-utenti"])
  }

  async presentToast(msg:string) {
    const toast = await this.toast.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
    
}

  


