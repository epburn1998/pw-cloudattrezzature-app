import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [

    //{ title: 'Home', url: '/menu-principale', icon: 'heart' },
  ];
  public labels = ['Famiglia', 'Amici', 'Note', 'Lavoro', 'Viaggio', 'Porto'];
  constructor(private router:Router) {}

  logout(){
    localStorage.clear();
    this.router.navigate(["/home"])
  }

  checkIfLogged(){
    if(localStorage.getItem("userEmail")==null)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}
