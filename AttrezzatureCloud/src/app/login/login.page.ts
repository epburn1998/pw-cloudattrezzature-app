import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../services/api-service.service'
import {HttpClient} from '@angular/common/http'
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public postData={
    email: '',
    password:''
  }

  dataAccesso: any;

  constructor(private apiService: ApiServiceService,private toastContr:ToastController,private router:Router) { }

  ngOnInit() {
  }

  login(){
    this.validateData(this.postData.email,this.postData.password);
  }

  controlloInput(){
    let email = this.postData.email.trim();
    let password= this.postData.password.trim();

    return (this.postData.email && this.postData.password && email.length>0 && password.length>0)
  }

  validateData(username:string,password:string){
    if(this.controlloInput()){
      this.apiService.getData().subscribe(data=>{
        //console.log(data);
        for (let i = 0; i < data.length; i++) {
          //controllo password
          if(data[i].emailUtente == this.postData.email && data[i].passUtente==this.postData.password){
            console.log("successo!");
            console.log(data[i])
            localStorage.setItem('userEmail',JSON.stringify(data[i].emailUtente));
            localStorage.setItem('userpass',JSON.stringify(data[i].passUtente));
            localStorage.setItem('userTeam',JSON.stringify(data[i].IDSquadra));
            localStorage.setItem('isCaposquadra',JSON.stringify(data[i].isCaposquadra));

            this.router.navigate(['menu-principale']);
            return;
          }
          
        }
        this.presentToast("Email o Password invalidi")
        
        
      },(error:any)=>{
        this.presentToast("Errore di Connessione. Riprova piú tardi")
      })
      
      
    }
    else{
      this.presentToast("Campi inseriti non validi")
    }
  }

  async presentToast(msg:string) {
    const toast = await this.toastContr.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  getData(){
    this.apiService.getData().subscribe(
      data => {
        //console.log(data);
        this.dataAccesso = data;
        console.log(this.dataAccesso);
        //this.validateData();
      },
      error => console.error(error),
      () => {
      }
      
      )
  }

  

  
}
