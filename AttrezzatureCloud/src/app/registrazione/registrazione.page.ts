import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../services/api-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrazione',
  templateUrl: './registrazione.page.html',
  styleUrls: ['./registrazione.page.scss'],
})
export class RegistrazionePage implements OnInit {

  constructor(private toastControl:ToastController,private apiService:ApiServiceService,private router:Router) { }

  public postData={
    nome: '',
    cognome:'',
    email:'',
    confirmEmail:'',
    pass:'',
    confirmPass:''
  }
  public emailAlreadyExists = false;
  public emailList = [];

  ngOnInit() {
  }

  controlInputs(){
    let nome = this.postData.nome.trim();
    let cognome = this.postData.cognome.trim();
    let email = this.postData.email.trim();
    let pass = this.postData.pass.trim();

    console.log(email)
    return (this.postData.nome && this.postData.cognome && 
      this.postData.email && this.postData.pass &&
      nome.length>0 && cognome.length>0 && 
      email.length>0 && email == this.postData.confirmEmail.trim()
      && pass.length>0 && pass== this.postData.confirmPass.trim())
  }

  async checkIfEmailExists(email:string){
    this.emailAlreadyExists = false;
    await this.getEmails();
    for(let i=0;i<this.emailList.length;i++){
        if(email.trim() === this.emailList[i]){
          console.log("Email giá presente");
          this.emailAlreadyExists= true;
          console.log(this.emailAlreadyExists);
          return this.emailAlreadyExists;
        }
    }
    console.log(this.emailAlreadyExists);
    return this.emailAlreadyExists;
  }

  async getEmails(){
    this.emailList = [];
    var data = await this.apiService.getData().pipe().toPromise();
    data.forEach(element => {
      console.log(element);
      this.emailList.push(element.emailUtente);
    });
    
  }

  async register(){
    if(this.controlInputs())
    {
      await this.checkIfEmailExists(this.postData.email);
      if(!this.emailAlreadyExists){
        this.apiService.createUser(this.postData.nome,this.postData.cognome
          ,this.postData.email,this.postData.pass).subscribe(data => {}
            ,(error:any)=> {
                this.presentToast('Errore imprevisto: '+ error.message);
            });
            //Accesso al menú con i nuovi dati
            localStorage.setItem('userEmail',JSON.stringify(this.postData.email));
            localStorage.setItem('userpass',JSON.stringify(this.postData.pass));
            this.router.navigate(['menu-principale']);
      }
      else{
        this.presentToast("Email giá presente");
      }
    }
    
  }

  

  async presentToast(msg:string) {
    const toast = await this.toastControl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
