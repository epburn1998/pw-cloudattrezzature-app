import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { element } from 'protractor';
import { audit } from 'rxjs/operators';
import { ApiServiceService } from '../services/api-service.service';
import { AlertController } from '@ionic/angular';
import { present } from '@ionic/core/dist/types/utils/overlays';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-pagina-lista-audit',
  templateUrl: './pagina-lista-audit.page.html',
  styleUrls: ['./pagina-lista-audit.page.scss'],
})
export class PaginaListaAuditPage implements OnInit {

  constructor(private apiService:ApiServiceService,private router:Router
    ,private toastContr:ToastController
    ,private alertCtrl:AlertController
    ,private scanner:BarcodeScanner) { }

  listaAttrezzature: Attrezzatura[];
  

  

  async ngOnInit() {
    var audits = await this.apiService.getAudits().pipe().toPromise();
    var attrezzatura = await this.apiService.getAttrezzatureInSquadra().pipe().toPromise();
    var listaAtt = await this.apiService.getAttrezzature().pipe().toPromise();    
  
    
    
    this.listaAttrezzature = [];

    if(localStorage.getItem("ArrayLista")!= null) //c'era giá una lista salvata
    {
      console.log("array list piena!");
      this.listaAttrezzature = JSON.parse(localStorage.getItem("ArrayLista"));
    }
    else //é stata aperta una nuova pagina 
    {
      console.log("array list vuota...")
      let i=0;
      attrezzatura.forEach(element => {

        //element.esitoAttrezzatura = audits[i].esitoAuditControllo;
        //element.checkVisivoStrumentale = audits[i].checkVisivoStrumentale;
        var audit = audits.filter(x=>x.IDAssegnazione == element.IDAssegnazione);
        var idAss = audit[0].IDAssegnazione
        

        if(audit.length>0){
          var idAudit = audit[0].IDAudit; 
          localStorage.setItem("idAudit",JSON.stringify(idAudit));
          


          var att = listaAtt.filter(x=>x.codiceAttrezzatura == element.codiceAttrezzatura)
          console.log(att[0].nomeAttrezzatura)
          var nomeAtt = att[0].nomeAttrezzatura;
        
        

          if(element.IDSquadra == Number(localStorage.getItem("userTeam"))){
            this.listaAttrezzature.push(new Attrezzatura(idAudit,idAss,element.IDSquadra
              ,element.codiceAttrezzatura,att[0].nomeAttrezzatura,audit.esitoAuditControllo
              ,audit.checkVisivoStrumentale,"","",""))
         
          }
        }
        else 
        {
          var att = listaAtt.filter(x=>x.codiceAttrezzatura == element.codiceAttrezzatura)
          console.log(att[0].nomeAttrezzatura)
          var nomeAtt = att[0].nomeAttrezzatura;
        
        

          if(element.IDSquadra == Number(localStorage.getItem("userTeam"))){
            this.listaAttrezzature.push(new Attrezzatura(idAudit,idAss,element.IDSquadra
              ,element.codiceAttrezzatura,att[0].nomeAttrezzatura,"Al momento non audit non richiesto"
              ,false,"","",""))
        }
      }
        
      });
      
      localStorage.setItem("ArrayLista",JSON.stringify(this.listaAttrezzature));
    }
  
  
  }


  async checkVisivoStrumentale(codAttrezzatura:number)
  {
     if(this.listaAttrezzature.find(x=>x.codiceAttrezzatura === codAttrezzatura)
     .checkVisivoStrumentale === true)
     {
       this.presentScannerAlert("Controllo richiesto"
       ,"Il fornitore ha richiesto un check Visivo per l'Audit. Scannerizzare il codice ora?"
       ,"Si","No")

     }
     else
     {
        this.dettagliAttrezzatura(codAttrezzatura);
     }
    
   
    
  }

  dettagliAttrezzatura(codAttrezzatura:number)
  {
    this.apiService.getAttrezzatura(codAttrezzatura).subscribe(res=>{
      if(res.codiceAttrezzatura!=null)
      {
        localStorage.setItem('attrezzaturaCodice',JSON.stringify(res.codiceAttrezzatura));
        localStorage.setItem('nomeAttrezzatura',JSON.stringify(res.nomeAttrezzatura));
        
        var idAss = this.listaAttrezzature.find(x=>x.codiceAttrezzatura===res.codiceAttrezzatura).idAssegnazione;
        localStorage.setItem('idAssegnazione',JSON.stringify(idAss));
         this.router.navigateByUrl('/pagina-dati-audit', { state: { codiceattrezzatura:res.codiceAttrezzatura,nomeAttrezzatura:res.nomeAttrezzatura} });
      }
      else
      {
        this.presentToast("Non é stata trovata nessuna attrezzatura con questo codice nel database. Usa un altro codice o contatta il fornitore");
      }
    },(error:any)=>{
      this.presentToast("Errore di connessione. Riprova piú tardi")
    }) 
  }

  async presentToast(msg:string) 
  {
    const toast = await this.toastContr.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async presentScannerAlert(header:string,message:string,buttonYes:string,buttonNo:string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: buttonNo,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Chiuso: blah');
            
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Apriti Scanner');
            this.scanner.scan().then(data=>{
              if(data.text==="")
              {
                //non fare nulla
                this.presentToast("Non é stato scannerizzato niente. Riprova.")
              }
              else  //stringa non vuota
              {
                //alert(JSON.stringify(data.text));
                if(!isNaN(Number(data.text)))
                {
                   this.apiService.getAttrezzatura(Number(data.text)).subscribe(res=>{
                    if(res.codiceAttrezzatura!=null)
                    {
                      this.router.navigateByUrl('/pagina-dati-audit', { state: { codiceattrezzatura:res.codiceAttrezzatura,nomeAttrezzatura:res.nomeAttrezzatura} });
                    }
                    else
                    {
                      this.presentToast("Non é stata trovata nessuna attrezzatura con questo codice nel database. Usa un altro codice o contatta il fornitore");
                    }
                  })
                }
                else
                {
                  this.presentToast("Codice QR inserito non valido! Riprova, o contatta il fornitore.")
                }
                //this.router.navigateByUrl('/paginaDatiAudit', { state: { codiceattrezzatura:data.text} });
              }
              
      
      
            },(error:any)=>{
              this.presentToast("Errore di connessione. Riprova piú tardi")
            })
            
          }
        }
      ]
    });

    await alert.present();
  }

}

export  class Attrezzatura {

  constructor(idAudit:number,idAssegnazione:number,idSquadra:number,codAtt:number,nomAtt:string,esitoAtt:string,checkVisivo:boolean,statAtt:string,descUtilizzo:string,noteAgg:string)
  {
    this.idAudit = idAudit;
    this.idAssegnazione = idAssegnazione;
    this.idSquadra = idSquadra;
    this.codiceAttrezzatura = codAtt;
    this.nomeAttrezzatura = nomAtt;
    this.esitoAttrezzatura = esitoAtt;
    this.checkVisivoStrumentale = checkVisivo;
    this.statoAttrezzatura = statAtt;
    this.descrizioneUtilizzo = descUtilizzo;
    this.noteAggiuntive = noteAgg;
  }

  idAudit: number;
  idAssegnazione:number;
  idSquadra:number;
  codiceAttrezzatura: number;
  nomeAttrezzatura: string = "";
  esitoAttrezzatura: string = "";
  checkVisivoStrumentale: boolean;
  statoAttrezzatura:string="";
  descrizioneUtilizzo:string="";
  noteAggiuntive:string="";
  


}
