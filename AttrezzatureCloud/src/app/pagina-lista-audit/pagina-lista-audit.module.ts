import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaginaListaAuditPageRoutingModule } from './pagina-lista-audit-routing.module';

import { PaginaListaAuditPage } from './pagina-lista-audit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaginaListaAuditPageRoutingModule
  ],
  declarations: [PaginaListaAuditPage]
})
export class PaginaListaAuditPageModule {}
