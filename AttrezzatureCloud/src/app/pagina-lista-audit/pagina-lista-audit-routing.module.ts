import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaginaListaAuditPage } from './pagina-lista-audit.page';

const routes: Routes = [
  {
    path: '',
    component: PaginaListaAuditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaginaListaAuditPageRoutingModule {}
