import { Component, OnInit } from '@angular/core';
import { AttachSession } from 'protractor/built/driverProviders';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-menu-principale-links',
  templateUrl: './menu-principale-links.page.html',
  styleUrls: ['./menu-principale-links.page.scss'],
})
export class MenuPrincipaleLinksPage implements OnInit {

  constructor(private apiService:ApiServiceService) { }

  disableAttrezziButton:boolean=false;

  auditMessage: string="";
  noOngoingAuditsMessage: string="Al momento nessun fornitore non ha richiesto audit.";
  ongoingAuditsMessage: string="Un fornitore ha richiesto un audit di recente. Apri il menú apposito per piú dettagli.";

  async ngOnInit() {
    console.log("dio")
    //GET per l'audit 
    //prende l'id della squadra e fa una richiesta con quell'id
    //al momento mettiamo un placeholder
    var auditSquadra = await this.apiService.getAudits().pipe().toPromise();

    auditSquadra.forEach(element => {
      
      if(element.IDSquadra === Number(localStorage.getItem("userTeam")))
      {
        if(element.esitoAuditControllo=="Attesa")
        {
          this.auditMessage=this.ongoingAuditsMessage;
        }
        else
        {
          this.auditMessage=this.noOngoingAuditsMessage;
        }
      }
      else
      {
        this.auditMessage=this.noOngoingAuditsMessage;
      }
      
    });

    //attrezzi
    var attSquad= await this.apiService.getAttrezzatureInSquadra().pipe().toPromise();
    console.log(attSquad);
    var nuovaAttSquad = attSquad.filter(x=>x.IDSquadra == Number(localStorage.getItem("userTeam")))
    
    if(nuovaAttSquad!=null){
      this.disableAttrezziButton = false;

    }
    else{
      this.disableAttrezziButton = true;
    }
    
  }

  isButtonDisabled(){
    if ( this.auditMessage === this.ongoingAuditsMessage)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  async isAttrezzatureButtonDisabled(){
    
    return true;
  }

}
