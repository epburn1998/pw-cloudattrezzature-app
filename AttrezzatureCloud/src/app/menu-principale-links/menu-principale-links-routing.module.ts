import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPrincipaleLinksPage } from './menu-principale-links.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPrincipaleLinksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPrincipaleLinksPageRoutingModule {}
