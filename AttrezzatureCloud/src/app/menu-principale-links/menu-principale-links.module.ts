import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPrincipaleLinksPageRoutingModule } from './menu-principale-links-routing.module';

import { MenuPrincipaleLinksPage } from './menu-principale-links.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPrincipaleLinksPageRoutingModule
  ],
  declarations: [MenuPrincipaleLinksPage]
})
export class MenuPrincipaleLinksPageModule {}
