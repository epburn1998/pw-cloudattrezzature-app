import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaginaDatiAuditPageRoutingModule } from './pagina-dati-audit-routing.module';

import { PaginaDatiAuditPage } from './pagina-dati-audit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaginaDatiAuditPageRoutingModule
  ],
  declarations: [PaginaDatiAuditPage]
})
export class PaginaDatiAuditPageModule {}
