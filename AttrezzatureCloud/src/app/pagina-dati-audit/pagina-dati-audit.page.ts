import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Attrezzatura } from '../pagina-lista-audit/pagina-lista-audit.page';

@Component({
  selector: 'app-pagina-dati-audit',
  templateUrl: './pagina-dati-audit.page.html',
  styleUrls: ['./pagina-dati-audit.page.scss'],
})
export class PaginaDatiAuditPage implements OnInit {

  idAudit:number;
  idAssegnazione:number;
  codiceAttrezzatura:number;
  nomeAttrezzatura:string;
  statoAttrezzatura:string;
  descUtilizzo:string;
  noteAggiuntive:string;

  
  listaAttrezzature: Attrezzatura[];

  constructor(private router:Router) { }

  ngOnInit() {
    var listParsed = JSON.parse(localStorage.getItem("ArrayLista"));
    this.listaAttrezzature = listParsed;
    
    console.log(this.listaAttrezzature);
    
    this.codiceAttrezzatura = Number(localStorage.getItem('attrezzaturaCodice'));
    this.idAssegnazione = Number(localStorage.getItem('idAssegnazione'));
    this.nomeAttrezzatura = localStorage.getItem('nomeAttrezzatura');
    this.idAudit = Number(localStorage.getItem('idAudit'));

    var idx = this.listaAttrezzature.findIndex(x=>x.codiceAttrezzatura == this.codiceAttrezzatura)
    
    this.statoAttrezzatura = this.listaAttrezzature[idx].statoAttrezzatura;
    this.descUtilizzo = this.listaAttrezzature[idx].descrizioneUtilizzo;
    this.noteAggiuntive = this.listaAttrezzature[idx].noteAggiuntive;
    

    localStorage.removeItem('nomeAttrezzatura');
    localStorage.removeItem('attrezzaturaCodice');
    
  }

  salvaRisultati()
  {
    console.log(this.statoAttrezzatura,this.descUtilizzo,this.noteAggiuntive);

    var attrezzatura = new Attrezzatura(this.idAudit,this.idAssegnazione,Number(localStorage.getItem("userTeam")),this.codiceAttrezzatura,this.nomeAttrezzatura,"Completato",false,this.statoAttrezzatura.toString(),this.descUtilizzo.toString(),this.noteAggiuntive.toString())
    var ind = this.listaAttrezzature.findIndex(x=>x.codiceAttrezzatura===this.codiceAttrezzatura);
    console.log(attrezzatura);
    this.listaAttrezzature[ind] = attrezzatura;
    localStorage.setItem("ArrayLista",JSON.stringify(this.listaAttrezzature));
    this.router.navigate(["/pagina-lista-audit"]);
  }

}
