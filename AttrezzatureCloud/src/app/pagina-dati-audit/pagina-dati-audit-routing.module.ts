import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaginaDatiAuditPage } from './pagina-dati-audit.page';

const routes: Routes = [
  {
    path: '',
    component: PaginaDatiAuditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaginaDatiAuditPageRoutingModule {}
