import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registrazione',
    loadChildren: () => import('./registrazione/registrazione.module').then( m => m.RegistrazionePageModule)
  },
  {
    path: 'pass-dimenticata',
    loadChildren: () => import('./pass-dimenticata/pass-dimenticata.module').then( m => m.PassDimenticataPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'menu-principale',
    loadChildren: () => import('./menu-principale/menu-principale.module').then( m => m.MenuPrincipalePageModule)
  },
  {
    path: 'pagina-dati-audit',
    loadChildren: () => import('./pagina-dati-audit/pagina-dati-audit.module').then( m => m.PaginaDatiAuditPageModule)
    ,data :{ codiceAttrezzatura:'',nomeAttrezzatura:''}
  },  {
    path: 'pagina-lista-audit',
    loadChildren: () => import('./pagina-lista-audit/pagina-lista-audit.module').then( m => m.PaginaListaAuditPageModule)
  },
  {
    path: 'conferma-invio-audit',
    loadChildren: () => import('./conferma-invio-audit/conferma-invio-audit.module').then( m => m.ConfermaInvioAuditPageModule)
  },
  {
    path: 'assegna-attrezzatura',
    loadChildren: () => import('./assegna-attrezzatura/assegna-attrezzatura.module').then( m => m.AssegnaAttrezzaturaPageModule)
  },
  {
    path: 'assegna-attrezzatura-squadra',
    loadChildren: () => import('./assegna-attrezzatura-squadra/assegna-attrezzatura-squadra.module').then( m => m.AssegnaAttrezzaturaSquadraPageModule)
  },
  {
    path: 'lista-utenti',
    loadChildren: () => import('./lista-utenti/lista-utenti.module').then( m => m.ListaUtentiPageModule)
  },
  {
    path: 'menu-principale-links',
    loadChildren: () => import('./menu-principale-links/menu-principale-links.module').then( m => m.MenuPrincipaleLinksPageModule)
  }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules },)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
