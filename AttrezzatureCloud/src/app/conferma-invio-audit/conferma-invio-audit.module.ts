import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfermaInvioAuditPageRoutingModule } from './conferma-invio-audit-routing.module';

import { ConfermaInvioAuditPage } from './conferma-invio-audit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfermaInvioAuditPageRoutingModule
  ],
  declarations: [ConfermaInvioAuditPage]
})
export class ConfermaInvioAuditPageModule {}
