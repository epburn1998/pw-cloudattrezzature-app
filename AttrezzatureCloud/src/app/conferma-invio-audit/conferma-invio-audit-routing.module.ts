import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfermaInvioAuditPage } from './conferma-invio-audit.page';

const routes: Routes = [
  {
    path: '',
    component: ConfermaInvioAuditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfermaInvioAuditPageRoutingModule {}
