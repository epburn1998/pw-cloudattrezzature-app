import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Attrezzatura } from '../pagina-lista-audit/pagina-lista-audit.page';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-conferma-invio-audit',
  templateUrl: './conferma-invio-audit.page.html',
  styleUrls: ['./conferma-invio-audit.page.scss'],
})
export class ConfermaInvioAuditPage implements OnInit {

  constructor(private router:Router,private apiService:ApiServiceService,private toastCtr:ToastController) { }

  listaAttrezzature:Attrezzatura[];

  ngOnInit() {
    this.listaAttrezzature = JSON.parse(localStorage.getItem("ArrayLista"))
    
  }

  async presentToast(msg:string) 
  {
    const toast = await this.toastCtr.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async invioAudit()
  {
    var idAudit = Number((localStorage.getItem("idAudit")));
    this.apiService.sendAudit(idAudit).pipe().toPromise();
    this.router.navigate(["/menu-principale-links"])
  }

}
